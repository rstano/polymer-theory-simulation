# Polymer theory simulation
Computational exercise for the Polymer Theory course (2022W) @ University of Vienna

Roman Staňo (`roman.stano@univie.ac.at`) & Ján Smrek (`jan.smrek@univie.ac.at`)

## Instructions

1. The final assignment has a form of two annotated jupyter notebooks which use a python core.
The simulations require several of standard python modules, all of which can be conveniently installed with [Anaconda3 toolkit](https://www.anaconda.com/products/individual).
After installation, download all of the files in this repository to your working station, and run the notebooks in your web browser.

1. You should start with the notebook named `simulate-polymer.ipynb`, which contains the description of your tasks and the framework for running the simulations.
For a short demonstration, just run all of the cells in this notebook using the default settings, before proceeding to the other notebook.

1. After getting yourself familiar with the first notebook, you should read the text in the notebook `analyze-outputs.ipynb`, which provides a framework for post-processing of the simulation data.
On your first reading, also in this notebook, you can run all of the cells with the default settings to get a better idea about the concepts of analysis.

## Support

The notebooks should provide all of the necessary tools to complete the assignment.
You will have to change values of various parameters, however no major intervention in the code is expected from you.
Also, you are not required to understand all of the codes in the core, however you are more than welcome to explore them.

If you have any questions, do not hesitate to reach out to the authors

## Acknowledgements

The authors thank Ondřej Maršálek (Charles University, Prague) for the inspiration for the code architecture - a lot was adopted from his course on Molecular dynamics.
We also acknowledge Peter Košovan (Charles University, Prague) for allowing us to use excerpts of his text on the statistical analysis of correlated time series.
