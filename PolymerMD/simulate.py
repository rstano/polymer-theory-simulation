from .interactions import *
from .io import *
from .utils import *
from contextlib import ExitStack
from numba.typed import List

import numpy as np
import pickle

def simulate(settings):
    '''
    Carry out the simulation:
    Args:
        settings: dictionary with keywords:
            N: chain length
            f: magnitude of elongation force
            temperature: temperature in kT
            architecture: 'flexible', 'rigid', 'block' or 'alternating'
            reload: boolean - restart simulation or start a new one
            t_max: total runtime
            stride_obs: stride for output of observables
            stride_xyz: stride for output of trajectory
            visualize: boolean - show the snapshots and time evolution of Ree
    '''

    print('PolymerMD')
    # extract settings
    N = settings['N']
    force = settings['f']
    architecture = settings['architecture']
    masses = dict(
      F = 1.0,
      R = 1.0,
    )
    
    # The following parametes are hard-coded because they should not be changed by the user
    dt = 0.01 # stable time-step of the propagator for equations of motions
    rho = 1.0e-9 # system density in N/sigma**3 is intentionally very low to ensure that the chain size is much smaller than the box size
    
    t_max = settings['t_max']
    T = settings['temperature']
    n_steps = int(t_max / dt)
    reload = settings['reload']
    stride_obs = int(settings['stride_obs']/dt)
    stride_xyz = int(settings['stride_xyz']/dt)
    if(stride_xyz < stride_obs):
        stride_xyz = stride_obs
        print("stride_xyz < stride_obs, change to stride_xyz =", stride_xyz)
    else:
        stride_xyz = int(stride_xyz/stride_obs)*stride_obs
    visualization = settings['visualize']
    
    # assert correct string values
    assert reload in [True, False], "error in the input value - only True and False are allowed for 'reload'"
    assert visualization in [True, False], "error in the input value - only True and False are allowed for 'visualize'"
    assert architecture in ['flexible', 'rigid', 'block', 'alternating'], "error in the input value - only 'flexible', 'rigid', 'block', 'alternating'"
    assert T > 0, "error in the input value - temperature must be positive"
    assert force >= 0, "error in the input value - the force must be non-negative"
    assert (type(N) is int) and N > 0, "error in the input value - number of monomers has to be a positive integer"
    
    # derived settings
    system_name = get_system_name(N = N, T = T, f = force, arch = architecture)
    settings['system_name'] = system_name
    print("system_name:", system_name)

    dth = 0.5 * dt # half-step for velocity-verlet setting
    L = (N / rho)**(1.0/3) # box length
    box = np.array((L, L, L))

    # architecture
    names = ['F',] * N
    if architecture == 'flexible':
      worms = None
    elif architecture == 'rigid':
      worms = List()
      for i in range(1,N-1):
        worms.append(i)
        names[i] = 'R'
    elif architecture == 'block':
      worms = List()
      for i in range(1,int(N/2)):
        worms.append(i)
        names[i] = 'R'
    elif architecture == 'alternating':
      worms = List()
      for i in range(1,int(N/2)):
        worms.append(2*i)
        names[2*i] = 'R'

    # prepare masses and their broadcasting version
    m = np.array([masses[name] for name in names], dtype=float)
    m_DOF = m[:, np.newaxis]    

    # INITIALIZE EVERYTHING
    # simulation time
    chk_fn = 'outputs/'+system_name+'_checkpoint.pk'
    if reload == True:
        t = pickle.load(open(chk_fn, 'rb'))
    else:
        t = 0.0
    # initial coordinates
    print("Reload:", reload)
    if reload == False:
        x = init_extended_rod(N, L)
    else:
        print('continue from a saved checkpoint')
        x = read_xyz("outputs/"+system_name+'_positions_final.xyz')[0][2]

    # thermostat settings
    k_B = 1.0 # Boltzmann constant in reduced units
    T_init = 1.0 # temperature of initial distribution
    T = T #temperature of the thermostat
    tau = 1.0 # thermostat time constant
    gamma = 1.0 / tau

    # constants for the thermostat
    A = np.exp(-gamma * dt)
    B = np.sqrt(k_B * T * (1-A**2) / m_DOF)
    
    # initial conditions - random thermal velocities
    v = np.random.normal(0, np.sqrt(k_B * T_init / np.repeat(m_DOF, 3, axis=1)))
    
    # update interactions for initial positions
    E_pot, dU_dx = interactions(x, box, force)
    a = - dU_dx / m_DOF
    
    # prepare output - initialize the output class
    output = Output(settings, system_name, x, v, names, worms, masses, k_B)
    
    # main loop with a context manager
    with ExitStack() as stack:
    
        # context manage output
        stack.enter_context(output)
        
        # loop over all propagation steps
        for i in steps(n_steps, dt, stride_xyz):
            # write output and visualization
            if(i%stride_obs ==0):
                output.calc_and_write_observables(i, t, E_pot)
                if(i%stride_xyz ==0):
                    output.write_xyz(i)
                    output.store_final_positions()
                    if(visualization):
                        output.flush_files()
                        output.visualize()
          
            # propagator
            v += a * dth # velocities at half-step
            x += v * dth # positions at half-step
            # thermostat
            v *= A
            v += B * np.random.normal(0.0, 1.0, size=v.shape)
            x += v * dth # positions at full-step -> it is important to split this so that the integrator is symplectic (Trotter theorem)
            x -= x[0,:] # shift the positions to the origin
            
            E_pot, dU_dx = interactions(x, box, force, worms = worms) # forces
            a[:] = - dU_dx / m_DOF # accelerations
            v += a * dth # velocities at full-step
          
            # update time
            t += dt
            
        # write output for the last frame
        i += 1
        if(i%stride_obs ==0):
            output.calc_and_write_observables(i, t, E_pot)
            if(i%stride_xyz ==0):
                output.write_xyz(i)
                output.store_final_positions()
            output.flush_files()
        if(visualization):
            output.visualize()
       
    # store final values of observables and positions
    pickle.dump(t,open(chk_fn, 'wb'))
    print("\nSimulation Finished\n")
