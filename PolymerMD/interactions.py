import numpy as np
import numba as nb

__all__ = ['interactions']

@nb.jit(nopython = True)
def mic_dist(x, pid1, pid2, box, boxinv):
    '''
    Evaluate the distance in minimal image convention in the periodic boundary conditions.
    Args:
        x: positions
        pid1: particle index 1
        pid2: particle index 2
    ''' 
    # displacement vector and distance in PBCs
    d2 = 0.0   # distance squared
    xij = np.zeros(3)
    for k in range(3):
        # loop over dimensions
        dxk = x[pid1, k] - x[pid2, k] # displacement
        dxk -= box[k] * round(dxk * boxinv[k]) # box-wraping
        xij[k] = dxk # minimal image convention displacements
        d2 += dxk * dxk # sum over dimensions
    return d2, np.sqrt(d2), xij

@nb.jit(nopython = True)
def interactions(x, box, force, k_bond = 100.0, r_0 = 1.0, k_bend = 1.0, cosphi0 = 1.0, worms = None):
    '''
    Calculate the interactions and return the forces.
    Args:
        x: positions
        box: box length
        force: magnitude of the external elongational force
        k_bond: stiffness of harmonic bond
        r_0: reference bond lenght with the minimum of potential
        k_bend: stiffness of angular harmonic cosine potentia
        cosphi0: cos(phi0) where phi0 is the reference bond angle with the minimum of potential
        worms: list of indices where angle potential is applied
    '''

    # prepare box inverse
    boxinv = 1.0 / box
    # allocate displacement vector
    xij = np.zeros(3)
    # accumulator for potential energy
    U = 0.0
    # accumulator for derivatives
    dU_dx = np.zeros_like(x)

    # apply the extensional force to the end monomers
    extension_vector = force * np.array((1,0,0))
    dU_dx[0,:] = extension_vector
    dU_dx[-1,:] = -extension_vector

    # number of atoms
    N = x.shape[0]

    # loop over all atoms but the last one
    # BONDED INTERACTIONS
    for i in range(N-1):
        d2, d, xij = mic_dist(x,i+1,i,box,boxinv) # assume that monomer (i) is connected to (i+1)
        u = 0.5 * k_bond * (d - r_0)**2 # harmonic potential
        U += u # add to total potential energy
      
        # loop over dimensions for force components
        for k in range(3):
            dU_dxk = k_bond * (d - r_0) * xij[k] / r_0 # force components
            dU_dx[i, k] -= dU_dxk
            dU_dx[i+1, k] += dU_dxk
    
    # BENDING INTERACTIONS
    if worms != None:
        for i in worms:
            # get the bond vectors
            d2_1, d_1, xij_1 = mic_dist(x,i,i+1,box,boxinv)
            d2_2, d_2, xij_2 = mic_dist(x,i+2,i+1,box,boxinv)

            # get the cosine
            c = np.dot(xij_1, xij_2)
            c /= d_1 * d_2

            # handle the extremal cases to prevent numerical instabilities
            if c > 1.0:
                c = 1.0
            if c < -1.0:
                c = -1.0
    
            # intermediate forces to project from polar to cartesian coordinates
            dcosphi = c - cosphi0
            tk = k_bend * dcosphi
            a = 2.0 * tk
            a11 = a*c / d2_1
            a12 = -a / (d_1 * d_2)
            a22 = a*c / d2_2
    
            # final forces on the monomers
            f10 = a11 * xij_1[0] + a12 * xij_2[0] 
            f11 = a11 * xij_1[1] + a12 * xij_2[1]
            f12 = a11 * xij_1[2] + a12 * xij_2[2]
            f30 = a22 * xij_2[0] + a12 * xij_1[0]
            f31 = a22 * xij_2[1] + a12 * xij_1[1]
            f32 = a22 * xij_2[2] + a12 * xij_1[2]
      
            # broadcast the forces
            dU_dx[i,0] += f10
            dU_dx[i,1] += f11
            dU_dx[i,2] += f12
            dU_dx[i+1,0] -= (f10+f30)
            dU_dx[i+1,1] -= (f11+f31)
            dU_dx[i+1,2] -= (f12+f32)
            dU_dx[i+2,0] += f30
            dU_dx[i+2,1] += f31
            dU_dx[i+2,2] += f32

            # log the energy
            u = 0.5 * k_bend * dcosphi**2
            U += u
    
    return U, dU_dx
