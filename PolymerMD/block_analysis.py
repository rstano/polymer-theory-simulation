import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from .io import *

__all__ = ["get_dt", "new_index", "block_analyze", "plot_time_trace"]

def new_index(data_frame, suffix, skip = ['time','step']):
    '''
    Get names of columns for outputs of block analysis.
    Args:
        data_frame: pandas data frame for observables
        suffix: 
        skip: 
    '''
    new_index = []
    for index in data_frame.index:
        if(index in skip):
            new_index.append(index)
        else:
            new_index.append(index+suffix)
    return(new_index)

def get_dt(data):
    '''
    Get the obs_stride to check the consistency of time series output.
    Args:
        data: pandas data frame for observables
    '''
    time = data['time']
    imax = data.shape[0]
    dt_init = time[1] - time[0]
    warn_lines = [];
    for i in range(1,imax):
        dt = time[i] - time[i-1]
        #print("i, t, dt:", i, time[i], dt)
        if(np.abs((dt_init - dt)/dt) > 0.01 ):
            warn_lines.append("Row {} dt = {} = {} - {} not equal to dt_init = {}\n".format(i, dt, time[i], time[i-1], dt_init)
            )
    if(len(warn_lines) > 20):
        print("\n*** Warning: Data file may be broken!*** \n\nFound {} data rows with inconsistent Delta t:\n".format(len(warn_lines))
             )
        for line in warn_lines:
            print(line)
    return dt
            
def block_analyze(filename, n_blocks=16, equil=0.1):
    '''
    Perform block analysis for error estimation.
    Args:
        filaname: observables file filename
        n_blocks: number of blocks
        equil: fraction of data to discard as equilibration
    '''
    params = get_params_from_file_name(filename)    
    full_data = pd.read_csv(filename,delim_whitespace=True) # read the full trajectory as pandas dataframe 
    dt = get_dt(full_data) # check that the data was stored with the same time interval dt
    drop_rows = int(full_data.shape[0]*equil) # calculate how many rows should be dropped as equilibration
    # drop the columns step and time, and rows that will be discarded as equlibration
    data = full_data.drop(columns=['step', 'time']).drop(range(0,drop_rows))
    # first, do the required operations on the remaining data
    n_samples = data.shape[0] # number of samples to be analyzed
    block_size = int(n_samples/n_blocks) # mean block size
    mean = data.mean() # calculate the mean values of each column
    var_all = data.var() # calculate the variance of each column
    params['Blocks'] = n_blocks
    params['B_size'] = block_size
    print("b:", n_blocks, "k:", block_size)
    
    # calculate the mean per each block    
    blocks = np.array_split(data,n_blocks) # split the data array into blocks of (almost) equal size
    block_means = [] # empty list that we will use to store the means per each block
    for block in blocks:
        block_mean = block.mean() # mean values of each column within a given block
        block_means.append(block_mean)            
    block_means = pd.concat(block_means, axis=1).transpose()
    
    # perform calculations using averages or individual data blocks
    var_mean = block_means.var() # variance of the block averages = variance of the mean
    err_mean = np.sqrt(var_mean) # standard error of the mean
    tau_int = dt*var_mean/var_all*block_size/2. # autocorrelation time in the unit of number of rows
    n_eff = block_size/(2*tau_int) # effective number of samples
    
    # modify the column names of the temporary results
    err_mean.index=new_index(err_mean,"Err")
    n_eff.index=new_index(n_eff,"Nef")
    tau_int.index=new_index(tau_int,"Tau")
    # first, concatenate the observables and alphabetically sort them by column names
    result = pd.concat( [ mean, err_mean, n_eff, tau_int ] ).sort_index(axis=0)
    # next, concatenate the results with simulation parameters to produce a human-readable output
    result = pd.concat( [ pd.Series(params), result] )
    return result

def plot_time_trace(filename, columns = 'all', save_pdf = False):
    '''
    Plot time series of quantities in the observables file.
    Args:
        filename: observables file
        columns: list of names of observables or 'all' for all of them
        save_pdf: if True, save plots as pdfs
    '''
    full_data = pd.read_csv(filename, delim_whitespace = True) # read the full trajectory observables as pandas dataframe 
    if columns == 'all':
        columns = full_data.columns.to_list()
    for x in ['time', 'step']:
        if(x in columns):
            columns.remove(x)
    if(save_pdf):
            directory = "outputs_pdf"
            if(os.path.isdir(directory)==False):
                os.mkdir(directory)
    n_cols = len(columns)
    for col in columns:
        fig = plt.figure(figsize = (20,7))
        plt.plot(
            full_data['time'],
            full_data[col],
            linewidth = 1.0,
            marker = 'o',
            markersize = 3,
            alpha = 0.8,         
            color = 'blue',
            label = col
        )
        plt.xlabel('time [$\\tau$]')
        if col == 'Epot' or col == 'Ekin':
            unit = 'k_B T'
        elif col == 'Reef' or col == 'Rbond':
            unit = '\\sigma'
        plt.ylabel('{:} [${:}$]'.format(col, unit))
        plt.title(filename)
        if(save_pdf):           
            pdf_name = filename.name.replace("observables.dat", "_"+col+".pdf")
            plt.savefig(os.path.join(directory,pdf_name))
        plt.show()
