import time
import numpy as np

__all__ = ['init_extended_rod', 'steps']

def init_extended_rod(N, L, b = 1, n = 0.05):
  """
  Create positions of particles on a line with uniform spacing with some noise.
  Args:
      N: number of monomeric units
      L: box length
      b: bond length - drift of the generator
      n: noise of the generator
  """
  pos = np.zeros((N,3))
  pos[:,0] += b * np.arange(N) + n * (np.random.random(N) - 0.5)
  return pos

def steps(n_steps, dt, stride = None):
  """
  Step iterator and progress printer.
  Args:
      n_steps: number of integration steps
      dt: timestep
      stride: period for output
  """
  if stride is None:
      stride = n_steps + 1

  # formatting strings
  fmt_progress = '\r{:04.1f} % completed | {:.2f} ms / tau | estimated time to complete: {:02d}m:{:02d}s'

  # stash start time
  t00 = time.time()
  t0 = t00

  for i_step in range(n_steps+1):
    # yield the item itself
    yield i_step
    # print progress and time per frame in milliseconds
    if i_step % stride == 0:
      t1 = time.time()
      time_rate = 1000 * (t1-t0) / stride / dt # in ms
      est_time_left = int(time_rate * (n_steps - i_step) * dt / 1000) # in seconds
      minutes = est_time_left // 60
      seconds = est_time_left - 60 * minutes
      
      print(fmt_progress.format(
        100.0 * i_step / n_steps, 
        time_rate,
        minutes,
        seconds
      ), end='', flush=True)
      t0 = t1
  i_step += 1
