import numpy as np
import os
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from IPython.display import clear_output
import pandas as pd

__all__ = ['write_xyz_frame', 'read_xyz_frame', 'read_xyz', 'get_system_name', 'get_params_from_file_name', 'Output']

def get_system_name(N, T, f, arch):
  '''
  Get the name of the system with given parameters.
  Args:
    N: number of monomers
    T: temperature
    f: force
    arch: architecture
  '''
  return r'N-{:03d}'.format(N)+'_architecture-{:}'.format(arch[:5])+'_T-{:.2f}'.format(T)+'_f-{:05.2f}'.format(f)

def write_xyz_frame(f_out, x, names, comment=''):
  '''
  Write one XYZ frame to an open file.
  Args:
    f_out: opened output file
    x: coordinates of positions
    names: array with types of atoms
    comment: comment
  '''
  N = x.shape[0]
  # number of atoms
  f_out.write('{:d}\n'.format(N))
  # comment line
  f_out.write(comment + '\n')
  # one line per atom
  for i in range(N):
    data = names[i], x[i, 0], x[i, 1], x[i, 2]
    f_out.write('{:s} {:12.6f} {:12.6f} {:12.6f}\n'.format(*data))

def read_xyz_frame(f_in):
  '''
  Read one frame from an open XYZ file. Returns `None` if there are no more frames.
  Args:
    f_in: opened input file
  '''
  line = f_in.readline()
  if line == '':
    return None
  N = int(line)
  comment = f_in.readline()[:-1]
  names = []
  data = []
  for i in range(N):
    items = f_in.readline().split()
    names.append(items[0])
    data.append([float(item) for item in items[1:]])
  return comment, names, np.array(data)

def read_xyz(fn_in):
  '''
  Read all frames from an XYZ file.
  Args:
    fn_in: name of the input file
  '''
  frames = []
  with open(fn_in) as f_in:
    while True:
      frame = read_xyz_frame(f_in)
      if not frame:
        break
      frames.append(frame)
    return frames

def get_params_from_file_name(file_name):
  '''
  Parse the observables file filename to get the parameters.
  Args:
    file_name: observables filename
  '''
  system_name = file_name.parts[-1].replace('_observables.dat', '')
  entries = system_name.split('_')
  params = {}
  for entry in entries:
    sp_entry = entry.split('-')
    if(sp_entry[0]=='N'):
      params[sp_entry[0]] = int(sp_entry[-1])
    else:
      params[sp_entry[0]] = sp_entry[-1]
  return params

class Output:
  '''
  Class for handling writing out data / visualization
  Args:
    settings: input dictionary for pmd.simulate
    system_name: filename root for the input variable
    x: positions
    v: velocities
    names: types of particles
    worms: list of indices where bending is applied'
    k_B: boltzmann constant
  '''

  def __init__(self, settings, system_name, x, v, names, worms, masses, k_B):
    self.system_name = system_name
    directory = 'outputs'
    if(os.path.isdir(directory)==False):
        os.mkdir(directory)
    self.fns={};
    self.fns['positions']=os.path.join(directory,system_name+'_positions.xyz')
    self.fns['positions_final']=os.path.join(directory,system_name+'_positions_final.xyz')
    self.fns['observables']=os.path.join(directory,system_name+'_observables.dat')

    self.reload = settings['reload']
    self.names = names
    self.worms = worms
    self.masses = masses
    self.N = settings['N']
    self.k_B = k_B
    self.reload = settings['reload']
    self.x = x
    self.v = v

    m = np.array([masses[name] for name in names], dtype=float)
    self.m_DOF = m[:, np.newaxis]
    self.fs_out = None

    # format string for one energy file line
    self.observable_names = ['step', 'time', 'Ekin', 'Epot', 'Reex', 'Rbond']
    # default 
    self.fmt_observables = '{:6d} {:10.3f} {:12.6f} {:12.6f} {:12.6f} {:12.6f} \n'
    
  def __enter__(self):
    fns = self.fns
    # open all files
    fs_out = {}
    for label, fn in fns.items():
      if fn is None:
        fs_out[label] = None
      else:
        if self.reload == True:
          fs_out[label] = open(fn, 'a')
        else:
          fs_out[label] = open(fn, 'w')
    self.fs_out = fs_out
    if(self.reload == False):
      for name in self.observable_names:
          fs_out['observables'].write(name+'  ') 
      fs_out['observables'].write('\n') 
    
  def __exit__(self, exception_type, exception_value, traceback):
    # close all files
    for label, f_out in self.fs_out.items():
      if f_out is not None:
        f_out.close()
    self.fs_out = None

  def flush_files(self):
    # flush all files
    for label, f_out in self.fs_out.items():
      if f_out is not None:
        f_out.flush()

  def calc_and_write_observables(self, i, t, E_pot):
    assert self.fs_out is not None, 'Need to enter context before running output.'
    fs_out = self.fs_out
    # calculate kinetic energy, temperature and end to end distance
    E_kin = (self.m_DOF * self.v**2).sum() / 2
    #T_kin = 2 * E_kin / (self.k_B * 3 * self.N) # kinetic temperature
    #Ree = self.x[0] - self.x[-1] # full 3D-distance
    #Ree = np.sqrt(np.dot(Ree, Ree))
    dReef = self.x[0,0] - self.x[-1,0] # distance of 1D-projection
    Reef = np.sqrt(np.dot(dReef, dReef))

    MPC = self.x.shape[0]
    r_bond = 0
    for m in range(MPC-1):
      r_b_tmp = self.x[m] - self.x[m+1]
      r_bond += np.sqrt(np.dot(r_b_tmp, r_b_tmp))
    r_bond /= (MPC - 1)
    #COM = np.mean(self.x, axis = 0) # center of mass
    #R_g2 = np.sum((self.x - COM)**2, axis = (0,1)) / self.N # gyration radius
    f_out = fs_out['observables']
    if f_out is not None:
      f_out.write(self.fmt_observables.format(i, t, E_kin, E_pot, Reef, r_bond))
      #f_out.write(self.fmt_observables.format(i, t, E_kin, E_pot, E_kin+E_pot, T_kin, Reef, r_bond))

  def write_xyz(self, i):
    assert self.fs_out is not None, 'Need to enter context before running output.'
    fs_out = self.fs_out
    # comment line for XYZ files
    comment = 'step {:d}'.format(i)
    # write XYZ files for positions
    names = self.names
    f_out_positions = fs_out['positions']
    if f_out_positions is not None:
      write_xyz_frame(f_out_positions, self.x, names, comment=comment)

  def store_final_positions(self):
    with open(self.fns['positions_final'], 'w') as f_out:
      write_xyz_frame(f_out, self.x, names=self.names)
      f_out.flush()
    self.flush_files()
    
  def visualize(self):
    # dirty solution for creating the animation - just clear the output and re-initialize the figure
    clear_output(wait=True)
    fig, ax = plt.subplots(1, 3, figsize = (15,7), gridspec_kw = {'width_ratios': [1,1,1]})
    
    # set the boundaries for xyz snapshot
    xyz = self.x
    Lxmin, Lxmax = min(xyz[:,0]), max(xyz[:,0])
    Lymin, Lymax = min(xyz[:,1]), max(xyz[:,1])
    Lzmin, Lzmax = min(xyz[:,2]), max(xyz[:,2])
    offset = 10

    # list with constants for xy and xz projections
    xyz_subplots = [
	[0,0,1, [Lxmin - offset, Lxmax + offset], [Lymin - offset, Lymax + offset], 'xy'],
	[1,0,2, [Lxmin - offset, Lxmax + offset], [Lzmin - offset, Lzmax + offset], 'xz'],
	]

    # plot xyz snapshots of the chain
    for xyz_subplot in xyz_subplots:
      index, xin, yin, xlim, ylim, label = xyz_subplot
      ax[index].axis('off')
      ax[index].set_title('${:}$-plane projection'.format(label))
      ax[index].set_xlim(xlim[0], xlim[1])
      ax[index].set_ylim(ylim[0], ylim[1])

      # always plot all of the segments with bonds
      ax[index].plot(
        xyz[:,xin],
        xyz[:,yin],
        linewidth = 3,
        color = 'blue',
        marker = 'o',
        markersize = 9,
        alpha = 0.8,
        label = 'polymer'
        )

      # if we have rigid segments, plot them again over the previous plot, but with different color and bigger points
      if self.worms != None:
        XYZ = xyz[self.worms]
        ax[index].plot(
          XYZ[:,xin],
          XYZ[:,yin],
          linewidth = 0,
          color = 'red',
          marker = 'o',
          markersize = 11,
          alpha = 0.8,
          label = 'polymer'
          )

    # read the dataframe with the observables
    data = pd.read_csv(self.fns['observables'], delim_whitespace = True)
    # plot the time series
    ax[2].set_xlabel('simulation time, $t\\ [\\tau]$')
    ax[2].set_ylabel('end-to-end distance along the elongation axis, $R_{ee}^x\\ [\\sigma]$')
    ax[2].plot(
      data['time'],
      data['Reex'],
      linewidth = 1,
      marker = 'o',
      markersize = 3,
      alpha = 0.8,
      color = 'black',
      label = 'instantaneous value'
      )
    # plot the running average
    frac = 0.9 # fraction of data to consider
    data_slice = data[-int(frac * data.shape[0]):]
    mean_slice = data_slice.mean()
    ax[2].plot(
      data_slice['time'],
      np.ones_like(data_slice['time']) * mean_slice['Reex'],
      linewidth = 5,
      marker = 'o',
      markersize = 1,
      alpha = 0.8,
      color = 'purple',
      label = 'sliding average ({:.1f}% discard): {:6.3f}'.format(100 * (1 - frac), mean_slice['Reex'])
      )
    ax[2].legend(loc = 'best')
    plt.show()
